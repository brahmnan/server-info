<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="tablesorter/jq.css" type="text/css" media="print, projection, screen" />
        <link rel="stylesheet" href="tablesorter/blue/style.css" type="text/css" id="" media="print, projection, screen" />
        <link rel="stylesheet" href="custom.css" type="text/css"/>

        <?php print $scripts; ?>
        <title><?php print $title; ?></title>
    </head>
    <body>
        <h1><?php print $title; ?></h1>
        <?php print $topmenu; ?>
        <?php print $content; ?>
    </body>
</html>