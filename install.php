<?php

/**
 * Файл установки Server info
 */

include 'config.php';
include 'safe_pdo.php';

if (defined('PDO_HOST')) {
    $dbh = db_connect();


    // Проверяем существование значений в таблице
    $query = "SELECT id FROM info limit 1";
    $sth = $dbh->prepare($query);
    $sth->execute();
    $data = $sth->fetchAll();

    if (!$data) {
        // Создаём таблицу
        install_info($dbh);
        print 'Таблица данных создана';
    } else {
        print 'Таблица данных уже создана';
    }
}

function install_info($dbh) {
    $sql = "CREATE TABLE IF NOT EXISTS  `info`(
				`id` int(11) unsigned NOT NULL auto_increment,				
				`cpu` int(11) NOT NULL DEFAULT '0',	
                                `date` int(11) NOT NULL DEFAULT '0',	
                                `info` text NOT NULL default '',                                                                                 
				PRIMARY KEY  (`id`)				
				) DEFAULT COLLATE utf8_general_ci;";
    $sth = $dbh->query($sql);
    return $sth;
}
