<?php
/* 
 * Добавление данных о севере из логов nginx в базу данных
 */

include 'config.php';
include 'safe_pdo.php';
include 'server_info.php';

if (defined('PDO_HOST')) {
    $dbh = db_connect();
    $si = new ServerInfo($dbh);
    $si->cron();
}
