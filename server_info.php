<?php

/**
 * Информация о сервере
 */
class ServerInfo {

    private $dbh;

    public function __construct($dbh) {
        $this->dbh = $dbh;
    }

    public function show() {
        $page = $_GET['page'];
        if ($page == 'current') {
            $this->showLog();
        } else {
            $this->getLog();
        }
    }

    public function getLog() {
        // Получаем период
        $period = $this->getPeriod();
        $count = $period * 60;

        //Получаем дату из базы данных
        $data = $this->getLogDataFromDB($count);
        if ($data) {

            //Компануем дату
            $periods = $this->getPeriods();
            $mod = $periods[$period]['mod'];

            $data = $this->groupData($data, $mod);

            //Отображаем дату
            $this->renderData($data);
        } else {
            print 'Нет данных';
        }
    }

    public function showLog() {
        $data = $this->getLogData();
        $this->renderLogData($data);
    }

    public function cron() {
        $data = $this->getLogData();
        $this->updateData($data);
    }

    private function getLogData() {

        $f = fopen(LOG_PATH, "r");
        $result = array();
        if ($f) {

            if (fseek($f, 0, SEEK_END) == 0) {//в конец файла -1 символ перевода строки
                $len = ftell($f);

                $offset = -2;
                $max_len = 5000000;
                $time_offset = 60; //60 сек.



                $time_log_first = '';

                //Ищим начало строки
                $stroka = '';
                for ($i = $len; $i > ($len - $max_len); $i--) {//5000 - предполагаемая макс. длина строки
                    $seec = fseek($f, $offset, SEEK_CUR);
                    if ($seec == -1) {
                        break;
                    }

                    $read = fread($f, 1);

                    if ($read == "\n") {//если встретился признак конца строки
                        $item = explode("|", $stroka);

                        $time_log = $item[1];
                        $time = strtotime($time_log);

                        if (!$time_log_first) {
                            $time_log_first = $time;
                        } else {
                            //print "$time_log_first $time<br />";
                            if (($time_log_first - $time_offset) > $time) {
                                break;
                            }
                        }

                        /* Log data
                          '$proxy_add_x_forwarded_for|$time_local|'
                          '$status|$request_length|$bytes_sent|$request_time|'
                          '$request|$http_referer|$http_user_agent';
                         */
                        $host = isset($item[9]) ? $item[9] : 'unknow';
                        $item_names = array(
                            'ip' => $item[0],
                            'time_local' => $item[1],
                            'status' => $item[2],
                            'request_length' => $item[3],
                            'bytes_sent' => $item[4],
                            'request_time' => $item[5],
                            'request' => $item[6],
                            'http_referer' => $item[7],
                            'http_user_agent' => $item[8],
                            'host' => $host,
                        );
                        $result[] = $item_names;
                        $stroka = '';
                    } else {
                        $stroka = $read . $stroka;
                    }
                }
            }

            fclose($f);
        }
        return $result;
    }

    function updateData($data) {
        if (sizeof($data)) {

            $time_first = strtotime($data[0]['time_local']);

            //Data by hosts
            $host = $this->sort_host_data($data);

            //CpuLoad
            $load = sys_getloadavg();
            $cpu = round($load[0], 2) * 100;

            $infostr = serialize($host);
            $date = $time_first;

            //Добавляем значения в таблицу
            $query = sprintf("INSERT INTO info (cpu,date,info) VALUES ('%d','%d','%s')", $cpu, $date, $infostr);
            $this->dbh->query($query);
        }
    }

    private function sort_host_data($data) {
        $host = array();
        $bots = $this->useronline_get_bots_custom();
        $def_host = array('req' => 0, 'ip' => '', 'status' => '', 'req_time' => 0, 'b_sent' => 0, 'bots_req' => 0, 'bots_ip' => '', 'bots_time' => 0);

        foreach ($data as $item) {

            //SERVER
            if (!isset($host['SERVER'])) {
                $host['SERVER'] = $def_host;
            }
            $host['SERVER']['req'] += 1;
            $host['SERVER']['ip'][$item['ip']] += 1;
            $host['SERVER']['status'][$item['status']] += 1;
            $host['SERVER']['req_time'] += $item['request_time'];
            $host['SERVER']['b_sent'] += $item['bytes_sent'];

            //host   
            if (!isset($host['host'])) {
                $host['host'] = $def_host;
            }
            $host[$item['host']]['req'] += 1;
            $host[$item['host']]['ip'][$item['ip']] += 1;
            $host[$item['host']]['status'][$item['status']] += 1;
            $host[$item['host']]['req_time'] += $item['request_time'];
            $host[$item['host']]['b_sent'] += $item['bytes_sent'];

            // Check For Bot
            foreach ($bots as $name => $lookfor) {
                if (stristr($item['http_user_agent'], $lookfor) !== false) {

                    $host['SERVER']['bots_req'] += 1;
                    $host['SERVER']['bots_ip'][$item['ip']] += 1;
                    $host['SERVER']['bots_time'] += $item['request_time'];

                    $host[$item['host']]['bots_req'] += 1;
                    $host[$item['host']]['bots_ip'][$item['ip']] += 1;
                    $host[$item['host']]['bots_time'] += $item['request_time'];

                    break;
                }
            }
        }

        //Убираем IP адреса
        foreach ($host as $name => $info) {
            $ip_count = isset($info['ip']) ? sizeof($info['ip']) : 0;
            $host[$name]['ip'] = $ip_count;

            $bot_count = isset($info['bots_ip']) ? sizeof($info['bots_ip']) : 0;
            $host[$name]['bots_ip'] = $bot_count;

            $host[$name]['req_time'] = '' . $host[$name]['req_time'];
            $host[$name]['bots_time'] = '' . $host[$name]['bots_time'];
        }
        return $host;
    }

    private function useronline_get_bots_custom() {
        $bots = array(
            'Googlebot' => 'Googlebot',
            'Google Bot' => 'Google Bot',
            'Googlebot-News' => 'Googlebot-News',
            'Googlebot-Image' => 'Googlebot-Image',
            'Googlebot-Video' => 'Googlebot-Video',
            'Googlebot-Mobile' => 'Googlebot-Mobile',
            'Mediapartners-Google' => 'Mediapartners-Google',
            'AdsBot-Google' => 'AdsBot-Google',
            'google' => 'Google',
            'MSN' => 'msnbot',
            'BingBot' => 'bingbot',
            'Alex' => 'ia_archiver',
            'Lycos' => 'lycos',
            'Ask Jeeves' => 'jeeves',
            'Altavista' => 'scooter',
            'AllTheWeb' => 'fast-webcrawler',
            'Inktomi' => 'slurp@inktomi',
            'Turnitin.com' => 'turnitinbot',
            'Technorati' => 'technorati',
            'Yahoo' => 'yahoo',
            'Findexa' => 'findexa',
            'NextLinks' => 'findlinks',
            'Gais' => 'gaisbo',
            'WiseNut' => 'zyborg',
            'WhoisSource' => 'surveybot',
            'Bloglines' => 'bloglines',
            'BlogSearch' => 'blogsearch',
            'PubSub' => 'pubsub',
            'Syndic8' => 'syndic8',
            'RadioUserland' => 'userland',
            'Gigabot' => 'gigabot',
            'Become.com' => 'become.com',
            'Baidu' => 'baidu',
            'Yandex' => 'yandex',
            'Rambler' => 'Rambler',
            'Mail.Ru' => 'Mail.Ru',
            'Webalta' => 'Webalta',
            'Quintura' => 'Quintura-Crw',
            'Turtle' => 'TurtleScanner',
            'Webfind' => 'webfind',
            'Aport' => 'Aport',
            'Amazon' => 'amazonaws.com',
            'Twitterbot' => 'Twitterbot',
            'applebot' => 'applebot',
            'AhrefsBot' => 'AhrefsBot',
            'SemrushBot' => 'SemrushBot',
        );

        return $bots;
    }

    private function groupData($data, $mod) {
        if ($mod == 1) {
            return $data;
        }

        $ret = array();
        $i = 1;
        if (sizeof($data) > 0) {
            $group = array();
            foreach ($data as $item) {
                //cpu
                $group['cpu'] += $item['cpu'] / $mod;
                //date
                if (!isset($group['date'])) {
                    $group['date'] = $item['date'];
                }
                //info
                $info = unserialize($item['info']);

                //hosts
                foreach ($info as $host => $hostinfo) {

                    //hostinfo
                    foreach ($hostinfo as $key => $value) {
                        if ($key == 'status') {
                            if (is_array($value)and sizeof($value)) {
                                foreach ($value as $skey => $sval) {
                                    $group['info'][$host]['status'][$skey] += $sval;
                                }
                            }
                            continue;
                        }

                        $group['info'][$host][$key] += $value;
                    }
                }
                $i++;
                if ($i >= $mod) {
                    $group['cpu'] = round($group['cpu'], 0);
                    $ret[] = $group;
                    $i = 0;
                    $group = array();
                }
            }
        }
        return $ret;
    }

    private function getLogDataFromDB($count) {
        $query = sprintf("SELECT cpu, date, info FROM info ORDER BY id DESC limit %d", $count);
        $sth = $this->dbh->prepare($query);
        $sth->execute();
        $data = $sth->fetchAll();
        $sth = null;
        return $data;
    }

    private function getPeriod() {
        $defperiod = 1;
        $current = $defperiod;

        $periods = $this->getPeriods();
        $period = (int) $_GET['per'];

        if ($period && isset($periods[$period])) {
            $current = $period;
        }

        return $current;
    }

    private function getPeriods() {
        $periods = array(
            1 => array('title' => 'Hour', 'mod' => 1),
            3 => array('title' => '3 hours', 'mod' => 2),
            6 => array('title' => '6 hours', 'mod' => 5),
            12 => array('title' => '12 hours', 'mod' => 10),
            24 => array('title' => 'Day', 'mod' => 15),
            168 => array('title' => 'Week', 'mod' => 60),
            720 => array('title' => '30 days', 'mod' => 1440)
        );

        return $periods;
    }

    private function getHostNameByReq() {
        if (isset($_GET['h'])) {
            $name = $_GET['h'];
        } else {
            $name = 'SERVER';
        }
        return $name;
    }

    private function getTopMenu() {
        $pages = array(
            'info' => 'Info',
            'current' => 'Current log',
        );
        $page = $_GET['page'];
        ?>
        <p>
            Pages: 
            <?php
            $i = 0;
            foreach ($pages as $key => $value) {
                $class = '';
                if ($page == $key) {
                    $class = ' class="active" ';
                }
                ?>
                <a href="?page=<?php print $key ?>"<?php print $class ?>><?php print $value ?></a> 
                <?php
                $i += 1;
                if ($i < sizeof($pages)) {
                    print ' | ';
                }
            }
            ?>
        </p>

        <?php
    }

    private function getPeriodMenu() {
        $periods = $this->getPeriods();
        $current = $this->getPeriod();
        $host_name = $this->getHostNameByReq();
        ?>
        <p>Time interval:
            <?php
            foreach ($periods as $key => $value) {
                $class = '';
                if ($current == $key) {
                    $class = ' class="active" ';
                }
                ?>
                |  <a href="?h=<?php print $host_name ?>&per=<?php print $key ?>"<?php print $class ?>><?php print $value['title'] ?></a>
            <?php }
            ?>            
        </p>
        <p>Group by <?php print $periods[$current]['mod']; ?> min.</p>
        <?php
    }

    private function hosts_total_info($hostnames, $hosts) {
        $total = array();
        foreach ($hostnames as $name => $count) {
            $hostdata = $hosts[$name];
            if (sizeof($hostdata > 0)) {
                /*    [1500224531] => Array
                  (
                  [req] => 2
                  [ip] => 1
                  [status] => Array
                  (
                  [200] => 2
                  )

                  [req_time] => 0.068
                  [b_sent] => 395234
                  [bots_req] => 0
                  [bots_ip] => 1
                  [bots_time] => 0
                  ) */
                foreach ($hostdata as $data) {
                    if (sizeof($data)) {
                        foreach ($data as $key => $value) {
                            if ($key == 'status') {
                                if (is_array($value) and sizeof($value)) {
                                    foreach ($value as $st => $n) {
                                        $total[$name][$key][$st] += $n;
                                    }
                                }
                                continue;
                            }
                            $total[$name][$key] += $value;
                        }
                    }
                }
            }
        }
        return $total;
    }

    private function getHostsPrecent($total_info, $from, $key) {
        /*
          [SERVER] => Array
          (
          [req] => 17851
          [ip] => 8352
          [req_time] => 7732.798
          [b_sent] => 453593907
          [bots_req] => 6513
          [bots_ip] => 1891
          [bots_time] => 3993.583
          )
         */
        if (!isset($total_info[$from][$key])) {
            return;
        }
        $from_data = $total_info[$from][$key];
        $ret = array();
        $ret[$from] = 100;
        if (sizeof($total_info) > 0) {
            foreach ($total_info as $server => $info) {
                if (isset($info[$key])) {
                    $ret[$server] = round($info[$key] / $from_data * 100, 2);
                }
            }
        }
        arsort($ret);
        return $ret;
    }

    private function renderHostPercent($hostsPrecent, $host_name) {
        if (sizeof($hostsPrecent) > 0) {
            $per = $this->getPeriod();
            $ret = array();
            foreach ($hostsPrecent as $host => $pr) {
                if ($pr >= 0.1) {
                    $active = '';
                    if ($host_name == $host) {
                        $active = ' class="active" ';
                    }
                    $ret[] = '<a href="?h=' . $host . '&per=' . $per . '"' . $active . '>' . $host . '<a/>:' . $pr . '%';
                }
            }
            print '<p>' . implode(', ', $ret) . '</p>';
        }
    }

    private function renderStatusHost($total_info, $host_name) {
        if (isset($total_info[$host_name])) {
            if (isset($total_info[$host_name]['status'])) {
                global $statuses;
                $statuses = $total_info[$host_name]['status'];
                if (sizeof($statuses) > 0) {
                    ksort($statuses);
                    $ret = array();
                    $stlocal = $statuses;
                    arsort($stlocal);

                    $all = 0;
                    foreach ($stlocal as $st => $v) {
                        $all += $v;
                    }

                    foreach ($stlocal as $st => $v) {

                        $persent = round($v * 100 / $all, 2);
                        if ($persent > 10) {
                            $persent = '<b>' . $persent . '</b>';
                        }
                        $ret[] = "<b>$st</b> ($v - $persent%)";
                    }
                    print '<p>Total requests: <b>' . $all . '</b>. Of them: ' . implode(', ', $ret) . '</p>';
                }
            }
        }
    }

    private function print_graphics($cpuarr, $hosts) {
        $period = $this->getPeriod();
        $host_name = $this->getHostNameByReq();
        $date_format = $period > 24 ? 'd.m H:i' : 'H:i';
        ?>
        <script type="text/javascript">
            google.charts.load('current', {'packages': ['corechart']});
            //cpu                
            google.charts.setOnLoadCallback(drawChartCPU);

            //request time
            google.charts.setOnLoadCallback(drawChartReqTime);

            //request
            google.charts.setOnLoadCallback(drawChartReq);

            //one request
            google.charts.setOnLoadCallback(drawChartOneReq);

            //one statuses
            google.charts.setOnLoadCallback(drawChartStatuses);

            google.charts.setOnLoadCallback(drawChartStatusesOther);

            function drawChartCPU() {
                var data = google.visualization.arrayToDataTable([
                    ['Time', 'CPU'],
        <?php
        $cpu_total = 0;

        foreach ($cpuarr as $key => $cpu) {
            $cpu_time = $cpu / 100;
            $cpu_total += $cpu;
            print "['" . gmdate($date_format, $key + 18000) . "', $cpu_time],";
        }
        $cpu_total_time = $cpu_total / 100;
        $cpu_average = round(($cpu_total / (sizeof($cpuarr))) / 100, 2);
        $cpu_title = 'CPU time. Total: ' . $cpu_total_time . '; Average: ' . $cpu_average;
        ?>
                ]);

                var options = {
                    title: '<?php print $cpu_title ?>',
                };

                var chart = new google.visualization.AreaChart(document.getElementById('cpu_div'));
                chart.draw(data, options);
            }

            function drawChartReqTime() {
                var data = google.visualization.arrayToDataTable([
                    ['Time', 'Total time', 'Bots time', 'Users time'],
        <?php
        $total_time = array();
        foreach ($cpuarr as $key => $cpu) {
            $req_time = isset($hosts[$host_name][$key]['req_time']) ? round($hosts[$host_name][$key]['req_time'], 2) : 0;
            $bots_time = isset($hosts[$host_name][$key]['bots_time']) ? round($hosts[$host_name][$key]['bots_time'], 2) : 0;
            $users_time = $req_time - $bots_time;

            $total_time['req'] += $req_time;
            $total_time['bot'] += $bots_time;
            $total_time['user'] += $users_time;

            print "['" . gmdate($date_format, $key + 18000) . "', $req_time, $bots_time, $users_time],";
        }
        $bots_pr = round(100 * $total_time['bot'] / $total_time['req'], 2);
        $user_pr = round(100 * $total_time['user'] / $total_time['req'], 2);
        $title = 'Request time total - ' . $total_time['req'] . ' sec. Bots time - ' . $total_time['bot'] . ' sec. (' . $bots_pr . '%). User time - ' . $total_time['user'] . ' sec. (' . $user_pr . '%)';
        ?>
                ]);
                var options = {
                    title: '<?php print $title ?>',
                };
                var chart = new google.visualization.AreaChart(document.getElementById('reqtime_div'));
                chart.draw(data, options);
            }

            function drawChartReq() {
                var data = google.visualization.arrayToDataTable([
                    ['Time', 'Total', 'Bots', 'Users'],
        <?php
        $total_req = array();
        foreach ($cpuarr as $key => $cpu) {
            $req_time = isset($hosts[$host_name][$key]['req']) ? $hosts[$host_name][$key]['req'] : 0;
            $bots_time = isset($hosts[$host_name][$key]['bots_req']) ? $hosts[$host_name][$key]['bots_req'] : 0;
            $users_time = $req_time - $bots_time;


            $total_req['req'] += $req_time;
            $total_req['bot'] += $bots_time;
            $total_req['user'] += $users_time;

            print "['" . gmdate($date_format, $key + 18000) . "', $req_time, $bots_time, $users_time],";
        }

        $bots_cpr = round(100 * $total_req['bot'] / $total_req['req'], 2);
        $user_cpr = round(100 * $total_req['user'] / $total_req['req'], 2);
        $title = 'Request total count - ' . $total_req['req'] . '. Bots count - ' . $total_req['bot'] . ' (' . $bots_cpr . '%). Users count - ' . $total_req['user'] . ' (' . $user_cpr . '%)';
        ?>
                ]);
                var options = {
                    title: '<?php print $title ?>',
                };
                var chart = new google.visualization.AreaChart(document.getElementById('req_div'));
                chart.draw(data, options);
            }

            function drawChartOneReq() {
                var data = google.visualization.arrayToDataTable([
                    ['Time', 'Total', 'User', 'Bots'],
        <?php
        $total_one = array();

        foreach ($cpuarr as $key => $cpu) {
            $req = isset($hosts[$host_name][$key]['req']) ? $hosts[$host_name][$key]['req'] : 0;
            $bots = isset($hosts[$host_name][$key]['bots_req']) ? $hosts[$host_name][$key]['bots_req'] : 0;

            $req_time = isset($hosts[$host_name][$key]['req_time']) ? $hosts[$host_name][$key]['req_time'] : 0;
            $bots_time = isset($hosts[$host_name][$key]['bots_time']) ? $hosts[$host_name][$key]['bots_time'] : 0;

            $one_total = 0;
            if ($req) {
                $one_total = round($req_time / $req, 2);
            }
            $one_bot = 0;
            if ($bots) {
                $one_bot = round($bots_time / $bots, 2);
            }
            $user_time = $req_time - $bots_time;
            $one_user = 0;
            $user_req = $req - $bots;
            if ($user_req) {
                $one_user = round($user_time / $user_req, 2);
            }
            $total_one['req'] += $one_total;
            $total_one['bot'] += $one_bot;
            $total_one['user'] += $one_user;

            print "['" . gmdate($date_format, $key + 18000) . "', $one_total, $one_user, $one_bot],";
        }

        $one_avg = round($total_one['req'] / sizeof($cpuarr), 2);
        $bots_avg = round($total_one['bot'] / sizeof($cpuarr), 2);
        $user_avg = round($total_one['user'] / sizeof($cpuarr), 2);
        $title = 'One request time. Total avg - ' . $one_avg . ' sec. Bots avg - ' . $bots_avg . ' sec. User avg - ' . $user_avg . ' sec';
        ?>
                ]);
                var options = {
                    title: '<?php print $title ?>',
                };
                var chart = new google.visualization.AreaChart(document.getElementById('onereq_div'));
                chart.draw(data, options);
            }

            function drawChartStatuses() {
                var data = google.visualization.arrayToDataTable([
        <?php
        global $statuses;
        $ret = '';
        $allowSt = array('200', '301', '302');
        if (sizeof($statuses)) {
            $ret .= '[';
            $ret .= "'Time',";
            foreach ($statuses as $st => $cnt) {
                if (in_array($st, $allowSt)) {
                    $ret .= "'$st ($cnt)',";
                }
            }
            $ret .= '],';


            foreach ($cpuarr as $key => $cpu) {
                $ret .= "[";
                //time

                $time = gmdate($date_format, $key + 18000);

                $ret .= "'$time',";
                foreach ($statuses as $st => $cnt) {
                    if (in_array($st, $allowSt)) {
                        $stval = isset($hosts[$host_name][$key]['status'][$st]) ? $hosts[$host_name][$key]['status'][$st] : 0;
                        $ret .= "$stval,";
                    }
                }
                $ret .= "],";
            }
        }
//$ret = str_replace(',]', ']', $ret);
        print $ret;

        $title = 'Satus: ';
        foreach ($statuses as $st => $cnt) {
            if (in_array($st, $allowSt)) {
                $st_pr = round(100 * $cnt / $total_req['req'], 2);
                $title .= "$st ($cnt - $st_pr%).  ";
            }
        }
        ?>
                ]);
                var options = {
                    title: '<?php print $title ?>',
                };
                var chart = new google.visualization.AreaChart(document.getElementById('statuses_div'));
                chart.draw(data, options);
            }


            function drawChartStatusesOther() {
                var data = google.visualization.arrayToDataTable([
        <?php
        global $statuses;
        $ret = '';
        if (sizeof($statuses)) {
            $ret .= '[';
            $ret .= "'Time',";
            foreach ($statuses as $st => $cnt) {
                if (!in_array($st, $allowSt)) {
                    $ret .= "'$st ($cnt)',";
                }
            }
            $ret .= '],';


            foreach ($cpuarr as $key => $cpu) {
                $ret .= "[";
                //time

                $time = gmdate($date_format, $key + 18000);

                $ret .= "'$time',";
                foreach ($statuses as $st => $cnt) {
                    if (!in_array($st, $allowSt)) {
                        $stval = isset($hosts[$host_name][$key]['status'][$st]) ? $hosts[$host_name][$key]['status'][$st] : 0;
                        $ret .= "$stval,";
                    }
                }
                $ret .= "],";
            }
        }
//$ret = str_replace(',]', ']', $ret);
        print $ret;

        $title = 'Satus other: ';
        foreach ($statuses as $st => $cnt) {
            if (!in_array($st, $allowSt)) {
                $st_pr = round(100 * $cnt / $total_req['req'], 2);
                $title .= "$st ($cnt - $st_pr%).  ";
            }
        }
        ?>
                ]);
                var options = {
                    title: '<?php print $title ?>',
                };
                var chart = new google.visualization.AreaChart(document.getElementById('statuses_other_div'));
                chart.draw(data, options);
            }
        </script>

        <div class = "graphics" style = "margin-left:-100px; margin-right:0">
            <div id = "cpu_div" style = "width: 100%; height: 300px;"></div>
            <div id = "reqtime_div" style = "width: 100%; height: 300px;"></div>
            <div id = "req_div" style = "width: 100%; height: 300px;"></div>
            <div id = "onereq_div" style = "width: 100%; height: 300px;"></div>
            <div id = "statuses_div" style = "width: 100%; height: 300px;"></div>
            <div id = "statuses_other_div" style = "width: 100%; height: 300px;"></div>
        </div>
        <?php
    }

    private function render_host_info_table($name, $info, $cpuarr) {
        if (sizeof($info) > 0) {
            
        } else {
            return;
        }

        $keys = array('req', 'ip', 'req_time', 'user_time', 'bots_time', 'user_%', 'bots_%', 'one_req', 'b_sent', 'bots_req', 'bots_ip', 'bots_time', 'bot_req', 'status');
        ?>
        <h2><?php print $name ?></h2>
        <table class="bordered tablesorter">
            <thead>
                <tr>
                    <th>Date</th>
                    <?php
                    if ($name == $host_name) {
                        print '<th>CPU</th>';
                    }
                    foreach ($keys as $key) {
                        print "<th>$key</th>";
                    }

                    global $statuses;
                    if (sizeof($statuses) > 0) {
                        foreach ($statuses as $key => $value) {
                            print "<th>$key</th>";
                        }
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($info as $time => $data) {
                    print '<tr>';
                    print '<td>' . gmdate('d.m H:i', $time + 18000) . '</td>';

                    if ($name == $host_name) {
                        $cpu = isset($cpuarr[$time]) ? $cpuarr[$time] : 0;
                        if ($cpu > 0) {
                            $cpu = $cpu / 100;
                        }
                        print '<td>' . $cpu . '</td>';
                    }

                    foreach ($keys as $key) {
                        $val = '';
                        if (isset($data[$key])) {
                            $val = $data[$key];
                            if ($key == 'status') {
                                if (sizeof($data[$key]) > 0) {
                                    $val = '';
                                    arsort($data[$key]);
                                    foreach ($data[$key] as $k => $v) {
                                        $val .= "$k:$v, ";
                                    }
                                    $val = preg_replace('/\, $/', '', $val);
                                }
                            }
                        } else {
                            $val = 0;
                            if ($key == 'one_req') {
                                if ($data['req_time']) {
                                    $val = round($data['req_time'] / $data['req'], 3);
                                }
                            } else if ($key == 'bots_%') {
                                if ($data['bots_time']) {
                                    $val = (round($data['bots_time'] / $data['req_time'], 4) * 100);
                                }
                            } else if ($key == 'user_%') {
                                if ($data['req_time']) {
                                    $val = (round(($data['req_time'] - $data['bots_time']) / $data['req_time'], 4) * 100);
                                }
                            } else if ($key == 'bot_req') {
                                if ($data['bots_time']) {
                                    $val = round($data['bots_time'] / $data['bots_req'], 3);
                                }
                            } else if ($key == 'user_time') {
                                if ($data['user_time']) {
                                    $val = round($data['req_time'] - $data['bots_time'], 2);
                                }
                            }
                        }
                        print '<td>' . $val . '</td>';
                    }

                    //status
                    if (sizeof($statuses) > 0) {
                        foreach ($statuses as $key => $value) {
                            $val = 0;
                            if (sizeof($data['status']) > 0) {
                                if (isset($data['status'][$key])) {
                                    $val = $data['status'][$key];
                                }
                            }
                            print '<td>' . $val . '</td>';
                        }
                    }
                    print '</tr>';
                }
                ?>

                <?php ?>
            </tbody>
        </table>
        <?php
    }

    private function renderData($data) {
        if (sizeof($data)) {
            $i_len = sizeof($data) - 1;
            $time_end = $data[0]['date'];
            $time_first = $data[$i_len]['date'];
            $host_name = $this->getHostNameByReq();

            $scripts = '<script type="text/javascript" src="tablesorter/jquery-latest.js"></script> 
                    <script type="text/javascript" src="tablesorter/jquery.tablesorter.min.js"></script>
                    <script type="text/javascript" id="js">$(document).ready(function () {$("table").tablesorter();});</script>
                    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>';

            $title = 'Server info';
            $topmenu = $this->getTopMenu();

            //Content
            ob_start();
            ?>
            <?php $this->getPeriodMenu(); ?>
            <p>Period: <?php print gmdate('d.m.y H:i:s', $time_first + 18000); ?> — <?php print gmdate('d.m.y H:i:s', $time_end + 18000); ?>. GMT +5</p>
            <?php
            //Получаем список хостов и готовим массив
            //$newdata = array();
            $hosts = array();
            $hostnames = array();
            $cpuarr = array();
            foreach ($data as $item) {
                if (is_string($item['info'])) {
                    $info = unserialize($item['info']);
                } else {
                    $info = $item['info'];
                }
                $cpu = $item['cpu'];
                $date = $item['date'];

                $cpuarr[$date] = $cpu;

                foreach ($info as $host => $hostinfo) {
                    $hosts[$host][$date] = $hostinfo;
                    if (isset($hostnames[$host])) {
                        $hostnames[$host] += 1;
                    } else {
                        $hostnames[$host] = 1;
                    }
                }

                //$newdata[] = array('cpu' => $cpu, 'date' => $item['date'], 'info' => $info);
            }
            $data = null;
            arsort($hostnames);
            ksort($cpuarr);
            //p_r($hostnames);
            //p_r($cpuarr);
            //p_r($hosts);
            //Hosts info
            $total_info = $this->hosts_total_info($hostnames, $hosts);

            //Показываем список хостов, с процентным соотношением по времени
            $hostsPrecent = $this->getHostsPrecent($total_info, 'SERVER', 'req_time');
            $this->renderHostPercent($hostsPrecent, $host_name);

            /* Array
              (
              [SERVER] => Array
              (
              [req] => 17851
              [ip] => 8352
              [status] => Array
              (
              [200] => 15096
              [499] => 65
              [302] => 1226
              [301] => 1024
              [444] => 207
              [304] => 81
              [404] => 83
              [500] => 29
              [403] => 25
              [408] => 5
              [206] => 10
              )

              [req_time] => 7732.798
              [b_sent] => 453593907
              [bots_req] => 6513
              [bots_ip] => 1891
              [bots_time] => 3993.583
              )

             */
            //render SERVER statuses

            $this->renderStatusHost($total_info, $host_name);

            //Server info
            $this->print_graphics($cpuarr, $hosts);


            //Host table

            $hostdata = $hosts[$host_name];
            $this->render_host_info_table($host_name, $hostdata, $cpuarr);
            
            $content = ob_get_contents();
            ob_end_clean();
            
            include 'page.php';
        }
    }

    function renderLogData($data) {
        if (sizeof($data)) {

            $time_first = strtotime($data[0]['time_local']);
            $time_end = strtotime($data[sizeof($data) - 1]['time_local']);

            //Data by hosts
            $host = $this->sort_host_data($data);

            //CpuLoad
            $load = sys_getloadavg();
            $cpu = round($load[0], 2) * 100;

            $infostr = serialize($host);
            $date = $time_first;

            $topmenu = $this->getTopMenu();
            $title = 'Current log';

            //Content
            ob_start();
            ?>
            <p>Period <?php print date('d.m.y H:i:s', $time_first); ?> — <?php print date('d.m.y H:i:s', $time_end); ?></p>
            <p>CPU <?php print $cpu; ?></p>
            <?php
            //Хосты
            if ($host) {
                if (sizeof($host) > 0) {
                    ksort($host);
                    //   p_r($host);
                    //Выводим данные
                    $host_names = array();
                    foreach ($host as $name => $info) {
                        $host_names[] = $name;
                    }
                    print '<p>Hosts: ' . implode(', ', $host_names) . '</p>';

                    foreach ($host as $name => $info) {
                        $this->render_host_info($name, $info);
                    }
                }
            }
            $content = ob_get_contents();
            ob_end_clean();

            include 'page.php';
        }
    }

    private function render_host_info($name, $info) {
        /*    [pandoraopen.ru] => Array
          (
          [req] => 304
          [ip] => 100
          [status] => Array
          (
          [200] => 275
          [302] => 19
          [301] => 9
          [304] => 1
          )

          [req_time] => 91.904
          [bots_req] => 161
          [bots_ip] => 22
          [bots_time] => 45.19
          ) */
        ?>
        <h2><?php print $name ?></h2>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Value</th>
                </tr>
            </thead>
            <tbody>           
                <tr><td>Total count</td><td><?php print isset($info['req']) ? $info['req'] : 0  ?></td></tr>
                <tr><td>Ip's</td><td><?php print isset($info['ip']) ? $info['ip'] : 0  ?></td></tr>
                <tr><td>Total time</td><td><?php print isset($info['req_time']) ? $info['req_time'] : 0  ?></td></tr>
                <tr><td>Bytes sent</td><td><?php print isset($info['b_sent']) ? $info['b_sent'] : 0  ?></td></tr>
                <tr><td>Bots count</td><td><?php print isset($info['bots_req']) ? $info['bots_req'] : 0  ?></td></tr>
                <tr><td>Bots Ip's</td><td><?php print isset($info['bots_ip']) ? $info['bots_ip'] : 0  ?></td></tr>
                <tr><td>Bots time</td><td><?php print isset($info['bots_time']) ? $info['bots_time'] : 0  ?></td></tr>
                <tr><td>Status:</td>
                    <td><?php
        if ($info['status']) {
            arsort($info['status']);
            foreach ($info['status'] as $key => $value) {
                print "$key:$value";
                print '<br />';
            }
        }
        ?></td>
                </tr>
            </tbody>
        </table>
        <?php
    }

}
